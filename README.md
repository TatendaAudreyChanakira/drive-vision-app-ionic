# Drive Vision App
The Drive Vision app created for Big & Open Data Hackathon 2018 in Windhoek, NA.

This app was created uder the theme "Better public transportation and safer roads"

The features include:
- View info on how long your journey should take on average time
- Map that pin points your location
- Alert near by authorities about accidents/congestion on the roads

The app is able to use a notification service to alert on accidents or congestion using via firebase.

A web control panel is under development, but it is currently not finalized. The wen control panel is used by the authorities who will view the demorgraphic overview of high risk accident routes. The web app also receives notifications about emergencies in oreder to ensure that a life is saved.

Future development for the application would include:
- Improving the app communication service between users
- Web control panel
- UI polishing
- Enable voice commands
- UI fixes for devices with very small screens

## Screenshots

![Screenshot_20180305-132033.png](https://bitbucket.org/repo/qEo6xGL/images/1739832279-Screenshot_20180305-132033.png) ![Screenshot_20180305-132041.png](https://bitbucket.org/repo/qEo6xGL/images/1767234019-Screenshot_20180305-132041.png)
![Screenshot_20180305-122326.png](https://bitbucket.org/repo/qEo6xGL/images/2177556761-Screenshot_20180305-122326.png)
![Screenshot_20180305-122331.png](https://bitbucket.org/repo/qEo6xGL/images/3820661973-Screenshot_20180305-122331.png)
![Screenshot_20180305-122731.png](https://bitbucket.org/repo/qEo6xGL/images/2090860548-Screenshot_20180305-122731.png)
import { Component } from '@angular/core';

import { NavController, AlertController } from 'ionic-angular';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController,public alertCtrl: AlertController) {
    
  }

  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'Drive safely',
      subTitle: 'You will arrive at your destination in the next 2 hrs, caution your route is a high risk zone.',
      buttons: ['OK']
    });
    alert.present();
  }



}
